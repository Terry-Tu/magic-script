package org.ssssssss.script.functions.linq;

import org.ssssssss.script.annotation.Comment;
import org.ssssssss.script.annotation.Function;
import org.ssssssss.script.functions.DateExtension;

import java.lang.reflect.Array;
import java.util.Date;

/**
 * Linq中的函数
 */
public class LinqFunctions {

	@Function
	@Comment("判断值是否为空")
	public Object ifnull(@Comment("目标值") Object target, @Comment("为空的值") Object trueValue) {
		return target == null ? trueValue : target;
	}

	@Function
	@Comment("日期格式化")
	public String date_format(@Comment("目标日期") Date target, @Comment("格式") String pattern) {
		return target == null ? null : DateExtension.format(target, pattern);
	}

	@Function
	@Comment("日期格式化")
	public String date_format(@Comment("目标日期") Date target) {
		return target == null ? null : DateExtension.format(target, "yyyy-MM-dd HH:mm:ss");
	}

	@Function
	@Comment("取当前时间")
	public Date now() {
		return new Date();
	}

	@Function
	@Comment("取当前时间戳(秒)")
	public long current_timestamp() {
		return System.currentTimeMillis() / 1000;
	}

	@Function
	@Comment("取当前时间戳(毫秒)")
	public long current_timestamp_millis() {
		return System.currentTimeMillis();
	}

	@Function
	@Comment("创建`int`数组")
	public int[] new_int_array(int size) {
		return new int[size];
	}

	@Function
	@Comment("创建`double`数组")
	public double[] new_double_array(int size) {
		return new double[size];
	}

	@Function
	@Comment("创建`float`数组")
	public float[] new_float_array(int size) {
		return new float[size];
	}

	@Function
	@Comment("创建`byte`数组")
	public byte[] new_byte_array(int size) {
		return new byte[size];
	}

	@Function
	@Comment("创建`char`数组")
	public char[] new_char_array(int size) {
		return new char[size];
	}

	@Function
	@Comment("创建`boolean`数组")
	public boolean[] new_boolean_array(int size) {
		return new boolean[size];
	}

	@Function
	@Comment("创建`long`数组")
	public long[] new_long_array(int size) {
		return new long[size];
	}

	@Function
	@Comment("创建`Object`数组")
	public Object[] new_array(int size) {
		return new Object[size];
	}

	@Function
	@Comment("创建`Object`数组")
	public <T> T[] new_array(Class<T> componentType,int size) {
		return (T[]) Array.newInstance(componentType, size);
	}
}
